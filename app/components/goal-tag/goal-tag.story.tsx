import React from "react"
import { View, ViewStyle } from "react-native"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { GoalTag } from "./goal-tag"

declare let module

const VIEWSTYLE: ViewStyle = {
  flexDirection: "row",
  width: "100%",
  justifyContent: "space-evenly",
}

storiesOf("GoalTag", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Use Case", () => (
    <Story>
      <UseCase text="Default (only text added)" usage="Default tag">
        <GoalTag tag="#example" color="red" />
      </UseCase>

      <UseCase text="Example" usage="Default tag">
        <View style={VIEWSTYLE}>
          <GoalTag tag="#swimming" color="crimson" />
          <GoalTag tag="#running" color="rgb(47,79,79)" />
          <GoalTag tag="#basketball" color="#3CB371" />
        </View>
      </UseCase>
    </Story>
  ))
