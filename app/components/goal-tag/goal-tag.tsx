import React from "react"
import { GoalTagProps } from "./goal-tag.props"
import { View, TouchableOpacity, Text, ViewStyle, TextStyle } from "react-native"

const ROOT: ViewStyle = {
  margin: 0,
  borderRadius: 100,
  backgroundColor: "red",
  alignSelf: "center",
  paddingLeft: 7,
  paddingRight: 7,
  paddingTop: 1,
  paddingBottom: 2,
}

const TEXT: TextStyle = {
  fontFamily: "Montserrat-Medium",
  fontSize: 14,
  color: "#ffffff",
}

export function GoalTag(props: GoalTagProps) {
  const { name, color, style, onPress } = props

  const onPressHandler = () => {
    if (onPress) {
      onPress({ name: name, color: color })
    }
  }

  return (
    <TouchableOpacity
      style={{ ...ROOT, ...{ backgroundColor: color }, ...style }}
      disabled={!onPress}
      onPress={() => onPressHandler()}
    >
      <Text style={{ ...TEXT }}>
        {name[0] !== "#" ? "#" : null}
        {name}
      </Text>
    </TouchableOpacity>
  )
}
