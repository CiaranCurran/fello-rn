import { string } from "mobx-state-tree/dist/internal"
import { ViewStyle } from "react-native"
import { TapGestureHandler } from "react-native-gesture-handler"

export interface GoalTagProps {
  /**
   * The tag of the goal being displayed, e.g. "#meditation"
   */
  name: string

  /**
   * The color of the goal in hex format, e.g. "#FFFFFF"
   */
  color: string

  /**
   * Optional: add styles or overwrite default style
   */
  style?: ViewStyle

  /**
   * Optional: onPress callback for interactivity
   */
  onPress?(goal: { name: string; color: string }): any
}
