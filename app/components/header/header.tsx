import React from "react"
import { View, ViewStyle, TextStyle } from "react-native"
import { HeaderProps } from "./header.props"
import { Button } from "../button/button"
import { Text } from "../text/text"
import { Icon } from "../icon/icon"
import { spacing } from "../../theme"
import { translate } from "../../i18n/"
import { HEADER_HEIGHT, REF } from "../../constants"
import { observable } from "mobx"

// static styles
const ROOT: ViewStyle = {
  zIndex: 100,
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "flex-start",
  backgroundColor: "blue",
  height: HEADER_HEIGHT,
}
const TITLE_VIEW: ViewStyle = {
  position: "absolute",
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  justifyContent: "center",
  alignItems: "center",
}
const TITLE: TextStyle = { fontFamily: "Montserrat-Bold", fontSize: 20 }
const RIGHT: ViewStyle = { flex: 1 }
const LEFT: ViewStyle = { flex: 1 }

/**
 * Header that appears on many screens. Will hold navigation buttons and screen title.
 */
export function Header(props: HeaderProps) {
  const { leftComponent, title, rightComponent, headerStyle } = props

  return (
    <View style={{ ...ROOT, ...headerStyle }}>
      {leftComponent}
      <View style={TITLE_VIEW}>
        <Text style={TITLE}>{title}</Text>
      </View>
      {rightComponent}
    </View>
  )
}
