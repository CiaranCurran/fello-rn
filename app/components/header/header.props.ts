import { ViewStyle, TextStyle } from "react-native"
import { IconTypes } from "../icon/icons"

export interface HeaderProps {
  /**
   * Component left of title e.g. back-button
   */
  leftComponent?: JSX.Element

  /**
   * header title
   */
  title?: string

  /**
   * Component right of title e.g. menu-button
   */
  rightComponent?: JSX.Element

  /**
   * Header styles
   */
  headerStyle?: ViewStyle
}
