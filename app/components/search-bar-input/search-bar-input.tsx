import React, { useEffect, useRef, useState } from "react"
import { View, ScrollView, TextInput, StyleSheet } from "react-native"
import { spacing } from "../../theme"

export const SearchBarInput = (props) => {
  const { selections, style, text, popSelection } = props
  const scrollRef = useRef()
  const [offset, setOffset] = useState(0)
  const onChangeTextHandler = (text) => {
    props.onChangeText(text)
  }

  return (
    <View style={{ width: "100%", height: 40, alignItems: "center" }}>
      <View style={styles.root}>
        <ScrollView
          ref={scrollRef}
          contentContainerStyle={styles.rootContainer}
          horizontal={true}
          scrollEnabled={true}
          nestedScrollEnabled={true}
          contentOffset={{ x: 200, y: 50 }}
          onContentSizeChange={(width, height) => {
            // eslint-disable-next-line no-unused-expressions
            scrollRef.current?.scrollTo({ x: 0 })
          }}
        >
          <View style={styles.children}>{selections}</View>
          <TextInput
            style={styles.input}
            onChangeText={(text) => {
              onChangeTextHandler(text)
            }}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === "Backspace") {
                if (text === "") {
                  popSelection()
                }
              }
            }}
            value={text}
            autoFocus={true}
            placeholder={"Search"}
            placeholderTextColor={"gray"}
          />
        </ScrollView>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  children: {
    flexDirection: "row",
    justifyContent: "center",
  },
  contentContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    backgroundColor: "transparent",
    color: "black",
    flex: 1,
    fontSize: 15,
    minWidth: 200,
  },
  root: {
    backgroundColor: "white",
    borderRadius: 20,
    display: "flex",
    flexDirection: "row",
    flex: 1,
    height: 40,
    overflow: "hidden",
    width: "95%",
    elevation: 10,
  },
  rootContainer: {
    display: "flex",
    paddingLeft: spacing[2],
  },
})

export default SearchBarInput
