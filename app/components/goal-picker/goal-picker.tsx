import React, { useEffect, useState } from "react"
import { StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { GoalTag } from "../"
import AutocompleteInput from "../auto-complete-input/auto-complete-input"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import { GoalPickerProps } from "./goal-picker.props"
import { color } from "../../theme"
import { RootState } from "../../models/store"
import { useSelector } from "react-redux"

const GoalPicker = ({
  style,
  onSubmitTextEntry,
  onSelectSuggestion,
  listContainerStyle,
  textInputStyle,
  placeholderTextColor,
  allGoals,
}: GoalPickerProps) => {
  const [query, setQuery] = useState("")
  const [show, setShow] = useState(false)

  // finds goals matching query string
  const findGoals = (query) => {
    if (query === "") {
      return allGoals
        .sort(() => 0.5 - Math.random())
        .slice(0, 5)
        .sort((a, b) => {
          if (a.count > b.count) {
            return -1
          } else if (a.count < b.count) {
            return 1
          }
          return 0
        })
    }
    console.log(query)
    const regex = new RegExp("^" + query.trim(), "i")

    console.log(regex)
    console.log("all" + allGoals)
    let filtered = allGoals.filter((goal) => goal.name.search(regex) >= 0)
    console.log(filtered)
    filtered = filtered
      .sort((a, b) => {
        if (a.count > b.count) {
          return -1
        } else if (a.count < b.count) {
          return 1
        }
        return 0
      })
      .slice(0, 5)
    return filtered
  }

  const toggleShow = () => {
    setShow(!show)
  }

  const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim()

  const currentGoals = findGoals(query)

  return (
    <View style={{ ...styles.autocompleteContainer, ...style }}>
      <AutocompleteInput
        inputContainerStyle={styles.autocompleteContainer}
        containerStyle={styles.containerStyle}
        listContainerStyle={[styles.listContainerStyle, listContainerStyle]}
        // data to show in suggestion, if matching don't show
        data={currentGoals.length === 0 ? [] : currentGoals}
        // default value if you want to set something in input
        defaultValue={query}
        hideResults={!show}
        onSubmitEditing={() => {
          setQuery("")
          onSubmitTextEntry(query)
        }}
        /* onchange of the text changing the state of the query which will trigger
    the findGoal method to show the suggestions */
        onChangeText={setQuery}
        placeholder="Search"
        maxLength={50}
        style={textInputStyle}
        renderTextInput={(props) => (
          <TextInput
            {...props}
            onFocus={toggleShow}
            onEndEditing={toggleShow}
            placeholder={"Goal"}
            placeholderTextColor={placeholderTextColor}
          />
        )}
        scrollViewProps={{
          style: {},
          // eslint-disable-next-line react/display-name
          renderItem: (item) => (
            // you can change the view you want to show in suggestion from here
            <TouchableOpacity
              key={item._id}
              style={styles.itemContainer}
              onPress={() => onSelectSuggestion(item)}
            >
              <View style={styles.topic}>
                <GoalTag name={item.name} color={item.color} />
                <Text style={styles.count}>{item.count}</Text>
              </View>
            </TouchableOpacity>
          ),
        }}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  autocompleteContainer: {
    borderRadius: 40,
    borderWidth: 0,
  },
  containerStyle: {
    padding: 5,
  },
  count: {
    color: "#575757",
    elevation: 10,
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    marginLeft: "auto",
    marginRight: "10%",
  },
  infoText: {
    fontSize: 16,
    textAlign: "center",
  },
  itemContainer: {
    height: 30,
    width: "100%",
  },
  itemText: {
    fontSize: 15,
    margin: 2,
    paddingBottom: 5,
    paddingTop: 5,
  },
  listContainerStyle: {
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "#ffffff",
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    paddingBottom: 10,
    paddingTop: 10,
    width: "90%",
  },
  topic: {
    alignItems: "center",
    flexDirection: "row",
    flex: 1,
    paddingLeft: 5,
    padding: 3,
    width: "100%",
  },
})

export { GoalPicker }
