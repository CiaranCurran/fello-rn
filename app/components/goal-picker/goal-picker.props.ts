import { ViewStyle } from "react-native"

export interface GoalPickerProps {
  /**
   * Additional container style. Useful for margins.
   */
  style?: ViewStyle | ViewStyle[]

  /**
   * Additional container style. Useful for margins.
   */
  onSubmitTextEntry?: any

  /**
   * List of goals to be suggested
   */
  allGoals: any

  /**
   * Additional container style. Useful for margins.
   */
  onSelectSuggestion?: any

  /**
   * Additional container style. Useful for margins.
   */
  listContainerStyle?: ViewStyle | ViewStyle[]

  /**
   * Additional container style. Useful for margins.
   */
  textInputStyle?: ViewStyle | ViewStyle[]

  /**
   * Additional container style. Useful for margins.
   */
  placeholderTextColor?: string
}
