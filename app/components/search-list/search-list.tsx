import React, { useEffect, useRef, useState } from "react"
import {
  ViewStyle,
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
} from "react-native"
import { useNavigation } from "@react-navigation/native"
import Img from "../../../assets/images/avatar.jpeg"
import { useBottomTabBarHeight } from "@react-navigation/bottom-tabs"
import ExpandableTags from "../expandable-tags/expandable-tags"
import SearchListItem from "../search-list-item/search-list-item"
import api from "../../services/api"
import { RootState } from "../../models/store"
import { useSelector } from "react-redux"

export const SearchList = () => {
  const navigation = useNavigation()

  const [data, updateData] = useState([] as any)
  const [page, setPage] = useState(1)
  const thisUserId = useSelector((state: RootState) => state.user._id)

  console.log("this" + JSON.stringify(thisUserId))
  // when data is updated we increment the page
  useEffect(() => {
    // we skip the initial render by waiting until the first fetch for data has been made
    if (data.length === 0) {
      return
    }
    console.log(page)
    setPage(page + 1)
  }, [data])

  useEffect(() => {
    const getUsers = async () => {
      const response = await api.getUsers({ page: page, limit: 10 })
      if (response.kind === "ok") {
        updateData(
          response.users.filter((user) => {
            console.log(user._id + " " + thisUserId)
            return user._id !== thisUserId
          }),
        )
      }
    }
    getUsers()
  }, [])

  const onEnd = () => {
    console.log("calling on page " + page)
    const getUsers = async () => {
      const response = await api.getUsers({ page: page, limit: 10 })
      if (response.kind === "ok") {
        updateData([...data, ...response.users.filter((user) => user._id !== thisUserId)])
      }
    }
    getUsers()
  }

  const renderItem = (item, index) => {
    return (
      <View key={index} style={styles.itemContainer}>
        <SearchListItem user={data[index]} />
      </View>
    )
  }

  const ListEmpty = () => {
    return (
      <View>
        <Text>No Results</Text>
      </View>
    )
  }
  return (
    <FlatList
      style={{ height: "100%", marginBottom: 0 }}
      contentContainerStyle={styles.container}
      renderItem={({ item, index }) => {
        return renderItem(item, index)
      }}
      data={data}
      ListEmptyComponent={ListEmpty}
      keyExtractor={(item, index) => index.toString()}
      onEndReachedThreshold={0.9}
      onEndReached={onEnd}
    />
  )
}

const styles = StyleSheet.create({
  column1: { alignItems: "center", flexDirection: "column" },
  column2: { display: "flex", flexDirection: "column", flex: 1, paddingLeft: 10 },
  container: {
    backgroundColor: "#f7faff",
    paddingTop: 20,
  },
  item: {
    backgroundColor: "white",
    borderRadius: 30,
    elevation: 5,
    flex: 1,
    flexDirection: "row",
    marginBottom: 10,
    minHeight: 90,
    padding: 5,
    width: "95%",
  },
  itemContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
    width: "100%",
  },
})
