import React from "react"
import { AvatarButtonProps } from "./avatar-button.props"
import { View, Text, ViewStyle, TextStyle, Image, ImageStyle, TouchableOpacity } from "react-native"
import { useNavigation } from "@react-navigation/native"
import { spacing } from "../../theme"
import Icons from "react-native-vector-icons/Feather"
import avatar from "../../../assets/images/avatar2.jpeg"

const ROOT: ViewStyle = {
  width: 60,
  height: 60,
  borderRadius: 50,
  backgroundColor: "blue",
  borderWidth: 2,
  borderColor: "rgba(255,255,255,0.6)",
  margin: spacing[3],
  elevation: 10,
}

const TEXT: TextStyle = {}

const IMAGE: ImageStyle = {
  width: "100%",
  height: "100%",
  borderRadius: 50,
}

export function AvatarButton(props: AvatarButtonProps) {
  const { style } = props
  const navigation = useNavigation()
  const navigateToProfile = () => navigation.navigate("")

  return (
    <View style={{ margin: spacing[3] }}>
      <Icons name={"menu"} color={"white"} size={40} onPress={navigateToProfile} />
    </View>
  )
}
