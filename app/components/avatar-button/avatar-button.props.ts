import { ViewStyle } from "react-native"

export interface AvatarButtonProps {
  /**
   * style props
   */
  style?: ViewStyle
}
