import React from "react"
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native"
import ExpandableTags from "../expandable-tags/expandable-tags"
import { useNavigation } from "@react-navigation/native"

const ChatsListItem = ({ data }) => {
  const navigation = useNavigation()
  const navigateToChat = () => {
    navigation.navigate("chatView", data)
  }

  return (
    <TouchableOpacity style={styles.item} onPress={() => navigateToChat()}>
      <View style={styles.column1}>
        <Image style={styles.avatar} source={data.avatar} />
      </View>
      <View style={styles.column2}>
        <Text style={styles.name}>{data.firstName + " " + data.surname}</Text>
        <Text style={styles.lastMessage}>{data.lastMessage}</Text>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  avatar: { borderRadius: 100, height: 60, width: 60 },
  column1: {
    alignItems: "center",
    flexDirection: "column",
  },
  column2: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    paddingLeft: 10,
    justifyContent: "center",
  },
  item: {
    elevation: 5,
    flex: 1,
    flexDirection: "row",
    width: "95%",
    backgroundColor: "white",
    borderRadius: 30,
    marginBottom: 10,
    padding: 5,
  },
  name: { color: "#071636", fontFamily: "Montserrat-Medium", fontSize: 16, marginBottom: 0 },
  status: { color: "#005ed9", fontWeight: "bold", marginTop: 0 },
  tagline: { color: "#293652", fontFamily: "Montserrat-Regular", marginBottom: 5 },
})

export default ChatsListItem
