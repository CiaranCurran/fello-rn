import React, { useLayoutEffect, useState } from "react"
import { View, Animated, TouchableOpacity, Easing, Text } from "react-native"
import { GoalTag } from "../goal-tag/goal-tag"
import Icon from "react-native-vector-icons/Feather"

const ExpandableTags = ({ tags }) => {
  const animationDuration = 300
  console.log(tags)

  const [animatedHeight, setAnimatedHeight] = useState(new Animated.Value(0))
  const [expanded, setExpanded] = useState(false)
  const [collapsing, setCollapsing] = useState(false)

  const expand = () => {
    // setAnimatedHeight(new Animated.Value(0))
    setCollapsing(false)
    setExpanded(true)
    Animated.timing(animatedHeight, {
      toValue: 100,
      duration: animationDuration,
      useNativeDriver: false,
      easing: Easing.ease,
    }).start()
  }

  const collapse = () => {
    setCollapsing(true)
    setExpanded(false)

    Animated.timing(animatedHeight, {
      toValue: tagsContainerHeight,
      duration: 400,
      useNativeDriver: false,
      easing: Easing.ease,
    }).start()
  }

  const interpolatedHeight = animatedHeight.interpolate({
    inputRange: [0, 100],
    outputRange: ["0%", "100%"],
  })

  // Keep track of when initial render is finished and new layout calculations complete
  const [layoutReady, setLayoutReady] = useState(false)

  // The maximum index of tags that will fit whole inside the tag container
  const [maxIndex, setMaxIndex] = useState(null)

  const [showDots, setShowDots] = useState(true)
  const [tagsContainerHeight, setTagsContainerHeight] = useState(null)

  let tagsContainerWidth
  let dotsWidth
  const widths = []

  const getContainerWidth = (e) => {
    const { height, width } = e.nativeEvent.layout
    setTagsContainerHeight(height)
    tagsContainerWidth = width
    let sum = dotsWidth
    widths.some((entry, i) => {
      if (sum + entry > tagsContainerWidth) {
        setMaxIndex(i)
        return true
      } else {
        sum += entry
        // in case the last item fits, there is no need for dots
        if (i === widths.length - 1) {
          setShowDots(false)
          setMaxIndex(i + 1)
        }
      }
    })
  }

  const getDotsWidth = (e) => {
    dotsWidth = e.nativeEvent.layout.width
  }

  const getMaxItems = (e, key) => {
    const width = e.nativeEvent.layout.width
    // if item would cause overflow set it as the max index
    widths.push(width)
  }

  useLayoutEffect(() => {
    if (maxIndex) {
      setLayoutReady(true)
    }
  }, [maxIndex])

  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        width: "100%",
      }}
      onLayout={layoutReady ? null : (e) => getContainerWidth(e)}
    >
      <Animated.View
        style={{
          width: "100%",
          flexDirection: "row",
          overflow: "visible",
          flexWrap: expanded ? "wrap" : "nowrap",
          alignItems: "center",
          height: expanded ? interpolatedHeight : collapsing ? animatedHeight : null,
        }}
      >
        {/* First we must render all of the tags */}
        {expanded ? (
          <>
            {tags.map((tag, i) => (
              <View
                key={i}
                style={{ padding: 1 }}
                onLayout={(e) => {
                  getMaxItems(e, i)
                }}
              >
                <GoalTag name={tag.name} color={tag.color} />
              </View>
            ))}
            <Icon name={"minimize-2"} color={"#47676b"} size={22} onPress={collapse} />
          </>
        ) : (
          <>
            {layoutReady
              ? tags.slice(0, maxIndex).map((tag, i) => (
                  <View key={i} style={{ padding: 1 }}>
                    <GoalTag name={tag.name} color={tag.color} />
                  </View>
                ))
              : tags.map((tag, i) => (
                  <View
                    key={i}
                    style={{ padding: 1, opacity: 0 }}
                    onLayout={(e) => {
                      getMaxItems(e, i)
                    }}
                  >
                    <GoalTag name={tag.name} color={"#33A2FF"} />
                  </View>
                ))}
            {showDots ? (
              <View
                style={{
                  display: "flex",
                  alignItems: "flex-end",
                  justifyContent: "flex-end",
                  alignSelf: "flex-end",
                }}
                onLayout={(e) => getDotsWidth(e)}
              >
                <Icon
                  style={{ height: 20 }}
                  name={"more-horizontal"}
                  size={25}
                  color={"#47676b"}
                  onPress={expand}
                />
              </View>
            ) : null}
          </>
        )}
      </Animated.View>
    </View>
  )
}

export default ExpandableTags
