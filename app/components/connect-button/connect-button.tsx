import * as React from "react"
import { useEffect, useState } from "react"
import { TouchableOpacity, TextStyle, ViewStyle, View, StyleSheet, Text } from "react-native"
import { useDispatch, useSelector } from "react-redux"
import { setUserData } from "../../models/reducers/user"
import api from "../../services/api"
import socket from "../../services/sockets"
import { SEND_CONNECTION_REQUEST, UPDATE_CONNECTION_STATUS } from "../../services/sockets/events"

export const ConnectButton = ({ to, from }) => {
  const [disabled, setDisabled] = useState(false)
  const { _id: thisUserID, connections } = useSelector((state: RootState) => state.user)
  const dispatch = useDispatch()

  // First off, check if the status should be friends or pending
  useEffect(() => {
    const getConnections = async () => {
      const result = await api.getConnections(thisUserID)
      if (result.kind === "ok") {
        dispatch(setUserData({ connections: result.connections }))
      }
    }
    getConnections()
  }, [])

  // send connection request
  const onPressConnect = () => {
    console.log("SENDING REQUEST")
    socket.emit(SEND_CONNECTION_REQUEST, { to, from })
  }
  // send connection request
  const onPressAccept = () => {
    socket.emit(ACCEPT_CONNECTION_REQUEST, { to, from })
  }

  let status = "not-connected"
  // Determine connection status
  if (connections?.friends.filter((user) => user._id.toString() === to).length) {
    status = "connected"
  } else if (connections?.sent.filter((user) => user._id.toString() === to).length) {
    status = "pending"
  } else if (connections?.received.filter((user) => user._id.toString() === to).length) {
    status = "received"
  }

  let statusText
  console.log(status)
  switch (status) {
    case "not-connected":
      statusText = "Connect"
      console.log("NOT CONNECTED")
      return (
        <TouchableOpacity
          disabled={disabled}
          onPress={onPressConnect}
          style={{ ...styles.button, ...styles.connect }}
        >
          <Text style={styles.text}>{statusText}</Text>
        </TouchableOpacity>
      )
    case "connected":
      statusText = "Connected"
      return (
        <TouchableOpacity disabled={disabled} style={{ ...styles.button, ...styles.connected }}>
          <Text style={styles.text}>{statusText}</Text>
        </TouchableOpacity>
      )
    case "pending":
      statusText = "Pending..."
      console.log("Should show pending")
      return (
        <TouchableOpacity disabled={disabled} style={{ ...styles.button, ...styles.pending }}>
          <Text style={styles.text}>{statusText}</Text>
        </TouchableOpacity>
      )
    case "received":
      statusText = "Accept Request"
      return (
        <TouchableOpacity disabled={disabled} onPress={onPressAccept} style={styles.button}>
          <Text style={styles.text}>{statusText}</Text>
        </TouchableOpacity>
      )
  }

  return (
    <TouchableOpacity disabled={disabled} onPress={onPress} style={styles.button}>
      <Text style={styles.text}>{statusText}</Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  accept: {
    backgroundColor: "#085dcc",
  },
  button: {
    alignItems: "center",
    borderRadius: 20,
    color: "white",
    height: 40,
    justifyContent: "center",
    marginBottom: 15,
    width: 150,
  },
  connect: {
    backgroundColor: "#085dcc",
  },
  connected: {
    backgroundColor: "#085dcc",
  },
  pending: {
    backgroundColor: "#9c9c9c",
  },
  text: {
    color: "white",
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
  },
})
