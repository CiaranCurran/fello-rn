import React, { useEffect, useState } from "react"
import {
  View,
  Text,
  TextInput,
  ViewStyle,
  TextStyle,
  requireNativeComponent,
  StyleSheet,
  Modal,
  TouchableOpacity,
} from "react-native"
import { result } from "validate.js"
import { spacing } from "../../theme"
import { GoalTag } from "../goal-tag/goal-tag"
import SearchBarInput from "../search-bar-input/search-bar-input"
import Icon from "react-native-vector-icons/MaterialIcons"
import { HEADER_HEIGHT } from "../../constants"
import { BlurView } from "@react-native-community/blur"
import api from "../../services/api"

const GOALS = {
  swimming: 3446,
  singing: 2349,
  sql: 9875,
  skiing: 123983,
  skydiving: 92,
  sleighing: 12,
  sailing: 2,
  brawling: 1545,
  biking: 1235,
  badmanton: 9221,
  baking: 123452134,
  basketball: 971831723,
  mediation: 72643234,
  music: 34123,
  martialarts: 0,
  mountainbiking: 0,
  dancing: 234,
  design: 3452,
  dueling: 4223,
  programming: 623434,
  piano: 5242,
  guitar: 234,
}

const COLORS = ["#C23B23", "#F39A27", "#EADA52", "#03C03C", "#579ABE", "#976ED7"]

const TAGS = Object.entries(GOALS).map(([goal, number]) => {
  const color = COLORS[Math.floor(Math.random() * COLORS.length)]
  return { goal, number, color }
})

export const SearchBar = (props) => {
  const { onClose } = props
  const [search, setSearch] = useState("")
  const [matches, setMatches] = useState([])
  const [selections, setSelections] = useState([])
  const [goals, setGoals] = useState([])

  const onChangeHandler = (text) => {
    console.log(text)
    setSearch(text)
  }

  useEffect(() => {
    const fetchData = async () => {
      const response = await api.getGoals()
      if (response.kind === "ok") {
        setGoals(response.goals)
      }
    }
    fetchData()
  })

  useEffect(() => {
    // search the list of goals and display the top 5 goals
    const pattern = new RegExp(`^${search}`, "i")
    let patternMatches = []
    patternMatches = goals.filter(({ name }) => {
      return pattern.test(name)
    })
    patternMatches.sort((a, b) => {
      return b.number - a.number
    })
    console.log(patternMatches)

    setMatches(patternMatches.slice(-5))
  }, [goals, search])

  return (
    <View style={styles.root}>
      <Modal transparent={true}>
        <View style={styles.modalContainer}>
          <View
            style={{
              overflow: "hidden",
              borderRadius: 20,
              display: "flex",
              position: "absolute",
              top: 10,
              left: 10,
              margin: 0,
              padding: 0,
              width: "80%",
              elevation: 10,
            }}
          >
            <BlurView style={styles.blurView} blurType={"light"} blurAmount={20}>
              <View style={styles.searchContainer}>
                <View style={styles.container}>
                  <SearchBarInput
                    style={styles.input}
                    onChangeText={(text) => onChangeHandler(text)}
                    selections={selections}
                    text={search}
                    popSelection={() => setSelections(selections.slice(0, -1))}
                  />
                  {matches.length > 0 ? (
                    <View style={styles.suggestionsContainer}>
                      {matches.map((match, index) => {
                        return (
                          <View key={match._id} style={styles.suggestionContainer}>
                            <View style={styles.suggestion}>
                              <GoalTag
                                name={match.name}
                                color={match.color}
                                onPress={({ name, color }) => {
                                  setSelections(
                                    selections.concat(
                                      <GoalTag
                                        key={match._id}
                                        color={color}
                                        name={name}
                                        style={{ marginLeft: 2, marginRight: 2 }}
                                      />,
                                    ),
                                  )
                                  setSearch("")
                                }}
                              />
                              <Text style={styles.number}>{match.count}</Text>
                            </View>
                            {index < matches.length - 1 ? <View style={styles.divider} /> : null}
                          </View>
                        )
                      })}
                    </View>
                  ) : null}
                </View>
              </View>
            </BlurView>
          </View>
          <TouchableOpacity style={styles.close} onPress={onClose}>
            <Icon name={"search-off"} color={"white"} size={40} />
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  )
}

interface Styles {
  root: ViewStyle
  container: ViewStyle
  input: TextStyle
  suggestionsContainer: ViewStyle
  suggestion: ViewStyle
  divider: ViewStyle
  number: TextStyle
  modalContainer: ViewStyle
  close: ViewStyle
  searchContainer: ViewStyle
  suggestionContainer: ViewStyle
  blurView: ViewStyle
}

const styles = StyleSheet.create<Styles>({
  blurView: {
    borderRadius: 100,
    display: "flex",
    height: "100%",
    width: "100%",
  },
  close: {
    alignItems: "flex-end",
    height: HEADER_HEIGHT,
    justifyContent: "center",
    marginLeft: "auto",
    marginRight: spacing[2],
    marginTop: 3,
    width: 50,
  },
  container: {
    alignItems: "center",
    backgroundColor: "rgba(255, 255, 255, 0.20)",
    borderRadius: 20,
    display: "flex",
    minHeight: 30,
    paddingTop: (50 - 40) / 2,
  },
  divider: {
    backgroundColor: "rgba(200, 200, 200, 0.9)",
    height: 1,
    marginLeft: spacing[2],
    marginRight: spacing[2],
    marginTop: spacing[3],
    width: "100%",
  },
  modalContainer: { flex: 1, flexDirection: "column" },
  number: {
    color: "gray",
    fontFamily: "Montserrat-Bold",
    marginLeft: "auto",
    marginRight: spacing[3],
  },
  root: {
    alignItems: "center",
    display: "flex",
    flex: 1,
    flexDirection: "column",
    height: "100%",
    overflow: "visible",
    paddingLeft: spacing[2],
    paddingRight: spacing[2],
    paddingTop: spacing[3],
  },
  searchContainer: { display: "flex", flex: 1, paddingTop: spacing[3] },
  suggestion: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
  },
  suggestionContainer: {
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "flex-start",
    paddingTop: spacing[2],
    width: "100%",
  },
  suggestionsContainer: {
    display: "flex",
    paddingBottom: spacing[3],
    paddingLeft: spacing[2],
    paddingRight: spacing[2],
    paddingTop: spacing[2],
    width: "100%",
  },
})
