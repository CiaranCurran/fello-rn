import React, { useRef, useState } from "react"
import {
  ViewStyle,
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
  TouchableOpacity,
  Animated,
  Easing,
} from "react-native"
import { useNavigation } from "@react-navigation/native"
import Img from "../../../assets/images/avatar.jpeg"
import { useBottomTabBarHeight } from "@react-navigation/bottom-tabs"
import ExpandableTags from "../expandable-tags/expandable-tags"
import ChatsListItem from "../chats-list-item/chats-list-item"

export const ChatsList = () => {
  const renderItem = (item, index) => {
    return (
      <View style={styles.itemContainer}>
        <ChatsListItem data={null} />
      </View>
    )
  }

  return (
    <FlatList
      style={{ height: "100%", marginBottom: 0, backgroundColor: "#f7faff" }}
      contentContainerStyle={styles.container}
      renderItem={({ item, index }) => {
        return renderItem(item, index)
      }}
      data={null}
      keyExtractor={(item, index) => index.toString()}
    />
  )
}

const styles = StyleSheet.create({
  column1: { alignItems: "center", flexDirection: "column" },
  column2: { display: "flex", flexDirection: "column", flex: 1, paddingLeft: 10 },
  container: {
    backgroundColor: "#f7faff",
    paddingTop: 20,
  },
  item: {
    elevation: 5,
    flex: 1,
    flexDirection: "row",
    width: "95%",
    minHeight: 90,
    backgroundColor: "white",
    borderRadius: 30,
    marginBottom: 10,
    padding: 5,
  },
  itemContainer: {
    alignItems: "center",
    flex: 1,
    justifyContent: "center",
    width: "100%",
  },
})
