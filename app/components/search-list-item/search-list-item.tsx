import React, { memo, useEffect, useState } from "react"
import { View, Text, TouchableOpacity, Image, StyleSheet } from "react-native"
import ExpandableTags from "../expandable-tags/expandable-tags"
import { useNavigation } from "@react-navigation/native"
import s3 from "../../utils/aws-s3"
import api from "../../services/api"

const SearchListItem = ({ user }) => {
  const navigation = useNavigation()
  const [goals, setGoals] = useState(null)

  const navigateToUser = () => {
    navigation.navigate("profileView", user)
  }

  const url = s3.getSignedUrl("getObject", {
    Bucket: "fello-dev",
    Key: user.avatar.Key,
    Expires: 100000,
  })

  useEffect(() => {
    const getGoals = async () => {
      console.log("getting goals")
      const response = await api.getGoalsByID(user.goals)
      console.log(response)
      if (response.kind === "ok") {
        console.log("got the goals")
        console.log(response.goals)
        setGoals(response.goals)
      }
    }
    getGoals()
  }, [])

  return (
    <TouchableOpacity style={styles.item} onPress={() => navigateToUser()}>
      <View style={styles.column1}>
        <Image
          style={styles.avatar}
          resizeMode={"cover"}
          source={{
            uri: url,
          }}
        />
        <Text style={styles.status}>Online</Text>
      </View>
      <View style={styles.column2}>
        <Text style={styles.name}>{user.firstName + " " + user.lastName}</Text>
        <Text style={styles.tagline}>{"this is a tagline"}</Text>
        <View style={{ flex: 1, justifyContent: "center" }}>
          {goals && <ExpandableTags tags={goals} />}
        </View>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  avatar: { borderRadius: 100, height: 60, width: 60 },
  column1: {
    alignItems: "center",
    flexDirection: "column",
  },
  column2: { display: "flex", flexDirection: "column", flex: 1, paddingLeft: 10 },
  item: {
    backgroundColor: "white",
    borderRadius: 30,
    elevation: 5,
    flex: 1,
    flexDirection: "row",
    marginBottom: 10,
    minHeight: 90,
    padding: 5,
    width: "95%",
  },
  name: { color: "#071636", fontFamily: "Montserrat-Medium", fontSize: 16, marginBottom: 0 },
  status: { color: "#005ed9", fontWeight: "bold", marginTop: 0 },
  tagline: { color: "#293652", fontFamily: "Montserrat-Regular", marginBottom: 5 },
})

export default memo(SearchListItem)
