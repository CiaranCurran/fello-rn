import { forEach } from "ramda"
import React, { useEffect, useReducer, useState } from "react"
import { StyleSheet, View, Keyboard } from "react-native"
import { TextInput, TouchableWithoutFeedback } from "react-native-gesture-handler"
import { color, spacing } from "../../theme"

export const CodeInput = ({ onComplete }) => {
  const [state, dispatch] = useReducer(
    (state, action) => {
      switch (action.type) {
        case "go-to-next":
          return {
            currentFocus: state.currentFocus + 1,
            code: state.code.concat(action.code),
          }
        case "go-to-previous":
          return {
            currentFocus: state.currentFocus - 1,
            code: state.code.filter((_, id) => id !== state.currentFocus),
          }
        case "clear-input":
          return {
            currentFocus: state.currentFocus,
            code: state.code.filter((_, id) => id !== state.currentFocus),
          }
        // for when we want to add the last digit without changing the focused input
        case "input-code":
          return {
            currentFocus: state.currentFocus,
            code: state.code.concat(action.code),
          }
        default:
          throw new Error()
      }
    },
    { currentFocus: 0, code: [] },
  )

  const [displaySubmit, setDisplaySubmit] = useState(false)

  const refs = []

  useEffect(() => {
    if (state.code.length === 6) {
      onComplete(state.code)
    }
  }, [state.code])

  useEffect(() => {
    if (refs) {
      refs[state.currentFocus].focus()
    }
  }, [state.currentFocus])

  console.log(state.code)

  // TODO: figure out how to persist the keyboard
  return (
    <TouchableWithoutFeedback>
      <View style={styles.container}>
        <TextInput
          onSubmitEditing={() => {}}
          autoFocus={true}
          editable={state.currentFocus === 0}
          maxLength={1}
          keyboardType="numeric"
          style={styles.input}
          onTextInput={(e) => {
            // if text is submitted focus on next input
            if (!e.nativeEvent.text) {
              console.log("clearing input")
              dispatch({ type: "clear-input" })
            } else {
              dispatch({ type: "go-to-next", code: e.nativeEvent.text })
            }
          }}
          ref={(input) => refs.push(input)}
        ></TextInput>
        <TextInput
          editable={state.currentFocus === 1}
          maxLength={1}
          keyboardType="numeric"
          style={styles.input}
          ref={(input) => refs.push(input)}
          onTextInput={(e) => {
            // if text is submitted focus on next input
            if (!e.nativeEvent.text) {
              console.log("clearing")
              dispatch({ type: "clear-input" })
            } else {
              dispatch({ type: "go-to-next", code: e.nativeEvent.text })
            }
          }}
          onKeyPress={(e) => {
            if (e.nativeEvent.key === "Backspace" && !state.code[state.currentFocus]) {
              dispatch({ type: "go-to-previous" })
            }
          }}
        ></TextInput>
        <TextInput
          editable={state.currentFocus === 2}
          maxLength={1}
          keyboardType="numeric"
          style={styles.input}
          ref={(input) => refs.push(input)}
          onTextInput={(e) => {
            // if text is submitted focus on next input
            if (!e.nativeEvent.text) {
              dispatch({ type: "clear-input" })
            } else {
              dispatch({ type: "go-to-next", code: e.nativeEvent.text })
            }
          }}
          onKeyPress={(e) => {
            if (e.nativeEvent.key === "Backspace" && !state.code[state.currentFocus]) {
              dispatch({ type: "go-to-previous" })
            }
          }}
        ></TextInput>
        <TextInput
          editable={state.currentFocus === 3}
          maxLength={1}
          keyboardType="numeric"
          style={styles.input}
          ref={(input) => refs.push(input)}
          onTextInput={(e) => {
            // if text is submitted focus on next input
            if (!e.nativeEvent.text) {
              dispatch({ type: "clear-input" })
            } else {
              dispatch({ type: "go-to-next", code: e.nativeEvent.text })
            }
          }}
          onKeyPress={(e) => {
            if (e.nativeEvent.key === "Backspace" && !state.code[state.currentFocus]) {
              dispatch({ type: "go-to-previous" })
            }
          }}
        ></TextInput>
        <TextInput
          editable={state.currentFocus === 4}
          maxLength={1}
          keyboardType="numeric"
          style={styles.input}
          ref={(input) => refs.push(input)}
          onTextInput={(e) => {
            // if text is submitted focus on next input
            if (!e.nativeEvent.text) {
              dispatch({ type: "clear-input" })
            } else {
              dispatch({ type: "go-to-next", code: e.nativeEvent.text })
            }
          }}
          onKeyPress={(e) => {
            if (e.nativeEvent.key === "Backspace" && !state.code[state.currentFocus]) {
              dispatch({ type: "go-to-previous" })
            }
          }}
        ></TextInput>
        <TextInput
          editable={state.currentFocus === 5}
          maxLength={1}
          keyboardType="numeric"
          style={styles.input}
          ref={(input) => refs.push(input)}
          onTextInput={(e) => {
            // if text is submitted focus on next input
            if (!e.nativeEvent.text) {
              dispatch({ type: "clear-input" })
            } else {
              Keyboard.dismiss()
              dispatch({ type: "input-code", code: e.nativeEvent.text })
            }
          }}
          onKeyPress={(e) => {
            if (e.nativeEvent.key === "Backspace" && !state.code[state.currentFocus]) {
              dispatch({ type: "go-to-previous" })
            }
          }}
        ></TextInput>
      </View>
    </TouchableWithoutFeedback>
  )
}

const styles = StyleSheet.create({
  container: { flexDirection: "row" },
  input: {
    alignItems: "center",
    borderColor: color.palette.black,
    borderRadius: 20,
    borderWidth: 4,
    color: color.palette.black,
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    height: 50,
    justifyContent: "center",
    margin: spacing[1],
    textAlign: "center",
    width: 50,
  },
})
