import React, { LegacyRef } from "react"
import { View } from "react-native"
export const HEADER_HEIGHT = 70
export const REF: LegacyRef<View> = React.createRef()
