import { GeneralApiProblem } from "./api-problem"

export interface Goal {
  name: string
  color: string
  count: number
}

// TODO: create user interface

export type GetUsersResult = { kind: "ok"; users: any[] } | GeneralApiProblem
export type GetUserResult = { kind: "ok"; user: any } | GeneralApiProblem
export type GetGoalsResult = { kind: "ok"; goals: Goal[] } | GeneralApiProblem
export type CreateUserResult = { kind: "ok"; user: any } | GeneralApiProblem
