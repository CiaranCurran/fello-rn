import { ApisauceInstance, create, ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "./api-problem"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import * as Types from "./api.types"
import { User } from "../../models/reducers/user"
import mime from "mime"

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  name: string
  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      headers: {
        Accept: "*/*",
      },
    })
  }

  /**
   * Gets a list of users.
   */
  async getUsers(params): Promise<Types.GetUsersResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users`, params)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const rawUsers = response.data.data
      return { kind: "ok", users: rawUsers }
    } catch {
      return { kind: "bad-data" }
    }
  }

  /**
   * Creates a user.
   */
  async createUser(user: User): Promise<Types.CreateUserResult> {
    // make the api call
    console.log(user)
    const body = new FormData()
    console.log("addign avater")
    const avatar = {
      uri: user.avatarLocal.path,
      name: user.avatarLocal.path.split("/").pop(),
      type: mime.getType(user.avatarLocal.path),
    }
    console.log(avatar)
    body.append("avatar", avatar)
    console.log("avarter added")
    // body.append("dateOfBirth", user.dateOfBirth)
    body.append("firstName", user.firstName)
    body.append("lastName", user.lastName)
    body.append("goals", JSON.stringify(user.goals.map((goal) => goal._id)))
    body.append("email", user.email)
    body.append("rooms", JSON.stringify(["adasda"]))
    body.append("userType", user.userType)
    body.append("timezone", user.timezone)
    body.append("bio", user.bio)

    console.log("posting to users")

    const response: ApiResponse<any> = await this.apisauce.post("/users", body)

    console.log(response)
    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const rawUser = response.data.data
      return { kind: "ok", user: rawUser }
    } catch {
      return { kind: "bad-data" }
    }
  }

  /**
   * Gets a single user by ID
   */
  async getUser(id: string): Promise<Types.GetUserResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users/${id}`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const resultUser: Types.User = {
        id: response.data.id,
        name: response.data.name,
      }
      return { kind: "ok", user: resultUser }
    } catch {
      return { kind: "bad-data" }
    }
  }

  /**
   * Returns list of all named goals
   */
  async getGoals(): Promise<Types.GetGoalsResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/goals`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const goals = response.data.data
      return { kind: "ok", goals: goals }
    } catch {
      return { kind: "bad-data" }
    }
  }

  async getGoalsByID(goals): Promise<Types.GetGoalsResult> {
    const body = new FormData()
    console.log(goals)
    body.append("ids", JSON.stringify(goals))

    const response: ApiResponse<any> = await this.apisauce.post("/goals/ids", body)
    console.log(response)

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      const goals = response.data.data
      return { kind: "ok", goals: goals }
    } catch {
      return { kind: "bad-data" }
    }
  }

  // TODO: implement result type
  async getConnectionStatus(to, from): Promise<any> {
    const response: ApiResponse<any> = await this.apisauce.get("/users/status", { to, from })

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      const status = response.data.status
      return { kind: "ok", status: status }
    } catch {
      return { kind: "bad-data" }
    }
  }

  // TODO: implement result type
  async getConnections(user): Promise<any> {
    const response: ApiResponse<any> = await this.apisauce.get("/users/connections", { user })

    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    try {
      const connections = response.data.connections
      return { kind: "ok", connections: connections }
    } catch {
      return { kind: "bad-data" }
    }
  }
}

export default new Api()
