import { useDispatch, useSelector } from "react-redux"
import socket from "."
import { setUserData } from "../../models/reducers/user"
import api from "../api/api"

const SocketManager = ({ children }) => {
  const thisUserID = useSelector((state: RootState) => state.user._id)
  const dispatch = useDispatch()

  // Only connect the socket after the user is setup (i.e. they've been given an ID)
  if (thisUserID) {
    socket.auth = { id: thisUserID }
    socket.connect()
  }

  // Listen for status updates
  socket.on("updateConnectionStatus", async () => {
    console.log("Received updateConnectionStatus event")
    const result = await api.getConnections(thisUserID)
    if (result.kind === "ok") {
      console.log("Conns:" + JSON.stringify(result.connections))
      dispatch(setUserData({ connections: result.connections }))
    }
  })

  // Clean up on disconnect
  socket.on("disconnect", () => {
    socket.removeAllListeners()
    socket.connect()
  })

  return children
}

export default SocketManager
