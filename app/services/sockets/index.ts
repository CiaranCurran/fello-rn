import { io } from "socket.io-client"
import { RECEIVE_CONNECTION_REQUEST } from "./events"
import { SOCKET_URL } from "@env"

const socket = io(SOCKET_URL, { autoConnect: false })

socket.on(RECEIVE_CONNECTION_REQUEST, () => {})

socket.on("connect", () => {
  console.log("CONNECTED TO SOCKET!")
})

export default socket
