/**
 * This is the navigator you will modify to display the logged-in screens of your app.
 * You can use RootNavigator to also display an auth flow or other user flows.
 *
 * You'll likely spend most of your time in this file.
 */
import React from "react"

import { createStackNavigator } from "@react-navigation/stack"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import {
  SearchScreen,
  ChatsScreen,
  PremiumScreen,
  ProfileViewScreen,
  ChatViewScreen,
  LoginScreen,
  SignupScreen,
  UserCreationName,
  UserCreationDate,
  UserCreationGoals,
  UserCreationBio,
  UserCreationSubmit,
  UserCreationAvatar,
} from "../screens"
import Ionicons from "react-native-vector-icons/Ionicons"
import { Text, View } from "react-native"
import { RootState } from "../models/store"
import { useSelector } from "react-redux"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */
export type PrimaryParamList = {
  home: undefined
  chats: undefined
  premium: undefined
  profileView: undefined
  chatView: undefined
  login: undefined
  signup: undefined
  userCreationName: undefined
  userCreationDate: undefined
  userCreationGoals: undefined
  userCreationBio: undefined
  userCreationSubmit: undefined
  userCreationAvatar: undefined
}

// Documentation: https://github.com/software-mansion/react-native-screens/tree/master/native-stack
const Stack = createStackNavigator<PrimaryParamList>()
const Tab = createBottomTabNavigator()

function HomeTabs() {
  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarActiveTintColor: "#ffffff",
        tabBarInactiveTintColor: "#6699CC",
        tabBarActiveBackgroundColor: "#0066ff",
        tabBarShowLabel: false,
        tabBarStyle: [
          {
            display: "flex",
          },
          null,
        ],
        // eslint-disable-next-line react/display-name
        tabBarIcon: ({ focused, color, size }) => {
          let iconName: string | undefined

          if (route.name === "search") {
            iconName = focused ? "search" : "search-outline"
          } else if (route.name === "chats") {
            iconName = focused ? "chatbox" : "chatbox-outline"
          } else if (route.name === "premium") {
            iconName = focused ? "star" : "star-outline"
          }

          // return <Ionicons name={iconName} size={size} color={color} />
          return <Ionicons name={iconName} size={size} color={color} />
        },
        headerShown: false,
      })}
    >
      <Tab.Screen name="search" component={SearchScreen} />
      <Tab.Screen name="chats" component={ChatsScreen} />
      <Tab.Screen name="premium" component={PremiumScreen} />
    </Tab.Navigator>
  )
}

const loggedIn = false

export function PrimaryNavigator() {
  // The app must watch the state so that the appropriate screens are shown for sign-up or onboarding
  const cognitoUserData = useSelector((state: RootState) => state.auth)

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
      }}
    >
      {!cognitoUserData.cognitoUserIsVerified && (
        <>
          <Stack.Screen name="signup" component={SignupScreen} />
          <Stack.Screen name="login" component={LoginScreen} />
        </>
      )}
      {!cognitoUserData.userCreationComplete && (
        <Stack.Group>
          <Stack.Screen name="userCreationName" component={UserCreationName} />
          <Stack.Screen name="userCreationDate" component={UserCreationDate} />
          <Stack.Screen name="userCreationGoals" component={UserCreationGoals} />
          <Stack.Screen name="userCreationAvatar" component={UserCreationAvatar} />
          <Stack.Screen name="userCreationBio" component={UserCreationBio} />
          <Stack.Screen name="userCreationSubmit" component={UserCreationSubmit} />
        </Stack.Group>
      )}
      <Stack.Screen name="home" component={HomeTabs} />
      <Stack.Screen name="profileView" component={ProfileViewScreen} />
      <Stack.Screen name="chatView" component={ChatViewScreen} />
    </Stack.Navigator>
  )
}

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["welcome"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
