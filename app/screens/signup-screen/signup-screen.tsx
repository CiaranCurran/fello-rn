import React, { useState } from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  StyleSheet,
  TextInput,
  KeyboardAvoidingView,
} from "react-native"
import { useNavigation } from "@react-navigation/native"
import { Button, CodeInput, codeInput, Header, Screen, Text, Wallpaper } from "../../components"
import { color, spacing } from "../../theme"
import { promisify } from "util"
import {
  CognitoUserPool,
  CognitoUserAttribute,
  CognitoUser,
  ISignUpResult,
  AuthenticationDetails,
} from "amazon-cognito-identity-js"
import { COGNITO_USER_POOL_ID, COGNITO_USER_POOL_WEB_CLIENT_ID } from "@env"
import { RootState } from "../../models/store"
import { setCognitoUserData } from "../../models/reducers/auth"
import { useSelector, useDispatch } from "react-redux"
import {
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from "react-native-gesture-handler"
import { StackNavigationProp } from "@react-navigation/stack"
import { PrimaryParamList } from "../../navigation"

const signUp = (
  userPool: CognitoUserPool,
  email: string,
  password: string,
): Promise<ISignUpResult> => {
  const emailAttribute = new CognitoUserAttribute({
    Name: "email",
    Value: email,
  })

  const attributes = [emailAttribute]
  const promisifiedSignUp: (
    username: string,
    password: string,
    userAttributes: CognitoUserAttribute[],
    validationData: CognitoUserAttribute[],
    clientMetaData: {},
  ) => Promise<any> = promisify(userPool.signUp).bind(userPool)

  return new Promise((resolve, reject) => {
    userPool.signUp(email, password, attributes, [], (err, res) => {
      if (err) {
        console.log(err.message)
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

const confirmRegistration = (cognitoUser: CognitoUser, verificationCode: string) => {
  return new Promise((resolve, reject) => {
    cognitoUser.confirmRegistration(verificationCode, false, (err, res) => {
      console.log("callback called")
      if (err) {
        console.log(err)
        reject(err)
      } else {
        resolve(res)
      }
    })
  })
}

export const SignupScreen = () => {
  const userPool = new CognitoUserPool({
    UserPoolId: COGNITO_USER_POOL_ID,
    ClientId: COGNITO_USER_POOL_WEB_CLIENT_ID,
  })
  const navigation = useNavigation<StackNavigationProp<PrimaryParamList>>()
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [verificationCode, setVerificationCode] = useState("")
  const cognitoUserData = useSelector((state: RootState) => state.auth)
  const dispatch = useDispatch()
  const [cognitoUser, setCognitoUser] = useState<CognitoUser>()
  const [usernameExistsException, setUsernameExistsException] = useState(false)
  const [passwordTooShortException, setPasswordTooShortException] = useState(false)
  const [invalidEmailException, setInvalidEmailException] = useState(false)
  const [code, setCode] = useState([])
  const [codeFailedError, setCodeFailedError] = useState(false)

  console.log(cognitoUserData)
  const onChangeEmail = (text) => {
    setEmail(text)
  }

  const onChangePassword = (text) => {
    setPassword(text)
  }

  const onSubmitEmail = async () => {
    clearExceptions()
    console.log(password.length)
    if (password.length < 7) {
      console.log("eval")
      setPasswordTooShortException(true)
      return
    }
    try {
      const res = await signUp(userPool, email, password)
      if (res) {
        dispatch(setCognitoUserData({ cognitoUsername: email }))
        setCognitoUser(res.user)
      }
    } catch (err) {
      if (err.name === "UsernameExistsException") {
        setUsernameExistsException(true)
      } else if (err.name === "InvalidPasswordException") {
        console.log(err)
      } else if (err.name === "InvalidParameterException") {
        if (err.message === "Invalid email address format.") {
          setInvalidEmailException(true)
        }
      }
    }
  }

  const clearExceptions = () => {
    setInvalidEmailException(false)
    setPasswordTooShortException(false)
    setUsernameExistsException(false)
  }

  const onSubmitVerificationCode = async () => {
    try {
      if (!code) {
        throw new Error("Verification code is undefined")
      }
      const res = await confirmRegistration(cognitoUser, code.join(""))
      console.log(res)
      cognitoUser.authenticateUser(
        new AuthenticationDetails({ Username: email, Password: password }),
        {
          onSuccess: (res) => {
            console.log("successful verification")
            dispatch(
              setCognitoUserData({
                cognitoAuthSession: JSON.stringify(res),
                cognitoUserIsVerified: true,
              }),
            )
            navigation.navigate("userCreationName")
          },
          onFailure: (err) => {
            setCodeFailedError(true)
            console.log(err)
          },
        },
      )
    } catch {
      setCodeFailedError(true)
    }
  }

  const onCompleteHandler = (code) => {
    setCode(code)
  }

  // if we haven't signed up yet display sign up inputs, otherwise, verify code
  if (!cognitoUser) {
    return (
      <View style={styles.main}>
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require("../../../assets/images/fello_line_signup.png")}
          />
          <Text style={styles.logoText}>fello</Text>
        </View>
        <View style={styles.inputContainer}>
          {invalidEmailException && <Text style={styles.warning}>Please enter a valid email</Text>}
          {usernameExistsException && (
            <Text style={styles.warning}>An account with this email already exists</Text>
          )}
          <TextInput
            style={styles.textInput}
            onChangeText={onChangeEmail}
            value={email}
            placeholder={"Email"}
          />
          {passwordTooShortException && (
            <Text style={styles.warning}>Password must be atleast 7 characters long</Text>
          )}
          <TextInput
            onChangeText={onChangePassword}
            placeholder={"Password"}
            value={password}
            secureTextEntry={true}
            style={styles.textInput}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={onSubmitEmail}>
          <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    )
  } else {
    return (
      <KeyboardAvoidingView behaviour={"height"} style={styles.main}>
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={require("../../../assets/images/fello_line_signup.png")}
          />
          <Text style={styles.logoText}>fello</Text>
        </View>
        <View style={{ marginTop: "20%", alignItems: "center" }}>
          <View style={{ width: "80%", marginBottom: "10%" }}>
            <Text style={styles.verificationHeader}>
              Please enter the verification code sent to your email.
            </Text>
          </View>
          {codeFailedError && <Text style={styles.warning}>Wrong verification code.</Text>}
          <CodeInput onComplete={onCompleteHandler} />
        </View>
        {code && (
          <TouchableOpacity
            style={{ ...styles.button, marginTop: "10%" }}
            onPress={onSubmitVerificationCode}
          >
            <Text style={styles.buttonText}>Sign Up</Text>
          </TouchableOpacity>
        )}
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    backgroundColor: color.palette.black,
    borderRadius: 100,
    height: 45,
    justifyContent: "center",
    width: 220,
  },
  buttonText: {
    color: color.palette.white,
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
  },
  inputContainer: {
    alignItems: "center",
    marginTop: "10%",
    width: "100%",
  },
  logo: {
    height: 125,
    width: 130,
  },
  logoContainer: {
    alignItems: "center",
    flexDirection: "row",
    marginTop: "20%",
  },
  logoText: {
    color: "#000",
    fontFamily: "Montserrat-Bold",
    fontSize: 60,
    marginLeft: spacing[3],
    marginTop: spacing[5],
  },
  main: {
    alignItems: "center",
    backgroundColor: "#fff",
    flex: 1,
    height: "100%",
  },
  textInput: {
    borderColor: color.palette.black,
    borderRadius: 20,
    borderWidth: 4,
    fontFamily: "Montserrat-Medium",
    fontSize: 20,
    marginBottom: spacing[4],
    paddingLeft: spacing[4],
    paddingRight: spacing[4],
    padding: spacing[1],
    width: "90%",
  },
  verificationHeader: {
    color: color.palette.black,
    fontFamily: "Montserrat-Medium",
    fontSize: 20,
    textAlign: "center",
  },
  warning: {
    color: color.palette.angry,
  },
})
