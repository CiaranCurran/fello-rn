import { observer } from "mobx-react-lite"
import React, { createRef, useEffect, useLayoutEffect, useRef, useState } from "react"
import { StyleSheet, View } from "react-native"
import { AvatarButton, Header } from "../../components"
import { ChatsList } from "../../components/chats-list/chats-list"

export const ChatsScreen = observer(function ChatsScreen() {
  return (
    <View style={{ height: "100%", marginBottom: 0 }}>
      <Header title="Chats" leftComponent={<AvatarButton style={styles.avatarButton} />} />
      <ChatsList />
    </View>
  )
})

const styles = StyleSheet.create({
  avatarButton: {},
})
