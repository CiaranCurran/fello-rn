import React from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  StyleSheet,
  Button,
} from "react-native"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, Wallpaper } from "../../components"
import { color, spacing, typography } from "../../theme"
import Img from "../../../assets/images/premium.jpeg"

export const PremiumScreen = observer(function PremiumScreen() {
  const navigation = useNavigation()
  const nextScreen = () => navigation.navigate("demo")

  return (
    <View style={styles.container}>
      <Image source={Img} style={styles.background} />
      <Text style={styles.header}>{`With Fello Pro,\nyou're more likely to succeed!`}</Text>
      <View style={styles.button}>
        <Text style={styles.buttonText}>LEARN MORE</Text>
      </View>
    </View>
  )
})

const styles = StyleSheet.create({
  background: { height: "100%", position: "absolute", width: "100%" },
  button: {
    backgroundColor: "#77c3ec",
    borderRadius: 100,
    paddingBottom: 20,
    paddingLeft: 50,
    paddingRight: 50,
    paddingTop: 20,
  },
  buttonText: {
    color: "white",
    fontFamily: "Montserrat-Medium",
    fontSize: 20,
  },
  container: { alignItems: "center", flex: 1, height: "100%", margin: 0, padding: 0 },
  header: {
    color: "white",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
    marginBottom: 40,
    marginTop: 300,
    paddingLeft: 50,
    paddingRight: 50,
  },
})
