import React, { useEffect, useState } from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  ScrollView,
  StyleSheet,
} from "react-native"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Button, GoalTag, Header, Screen, Text, Wallpaper, ConnectButton } from "../../components"
import { color, spacing, typography } from "../../theme"
import Icon from "react-native-vector-icons/Ionicons"
import s3 from "../../utils/aws-s3"
import api from "../../services/api"
import { useSelector } from "react-redux"
import { t } from "i18n-js"

export const ProfileViewScreen = ({ route }) => {
  const navigation = useNavigation()
  const nextScreen = () => navigation.navigate("demo")
  const [goals, setGoals] = useState([])
  const thisUserId = useSelector((state: RootState) => state.user._id)

  useEffect(() => {
    const getGoals = async () => {
      console.log("getting goals")
      const response = await api.getGoalsByID(user.goals)
      console.log(response)
      if (response.kind === "ok") {
        console.log("got the goals")
        console.log(response.goals)
        setGoals(response.goals)
      }
    }
    getGoals()
  }, [])

  const user = route.params

  const backButtonPress = () => {
    navigation.goBack()
  }

  const avatarUrl = s3.getSignedUrl("getObject", {
    Bucket: "fello-dev",
    Key: user.avatar.Key,
    Expires: 100000,
  })

  return (
    <View style={{ flex: 1 }}>
      <Header
        title={user.firstName + " " + user.lastName}
        leftComponent={
          <Icon
            style={{ marginLeft: 20 }}
            name={"arrow-back"}
            size={30}
            color={"white"}
            onPress={backButtonPress}
          />
        }
      />
      <ScrollView contentContainerStyle={styles.scrollContent} style={styles.scroll}>
        <View style={styles.cover}>
          <View style={styles.avatarContainer}>
            <Image style={styles.avatar} source={{ uri: avatarUrl }} />
          </View>
        </View>
        <Text style={styles.tagsHeader}>{`Goals`}</Text>
        <View style={styles.tagsContainer}>
          {goals.map((goal, index) => (
            <View key={index} style={styles.tag}>
              <GoalTag name={goal.name} color={goal.color} />
            </View>
          ))}
        </View>
        <ConnectButton to={user._id} from={thisUserId} />
        <Text style={styles.sectionHeader}>{`About ${user.firstName}`}</Text>
        <View
          style={{
            width: "100%",
            paddingLeft: 20,
            paddingRight: 20,
            display: "flex",
            flexDirection: "row",
          }}
        >
          <View style={styles.section}>
            <Text style={styles.bio}>{user.bio}</Text>
          </View>
        </View>
        <Text style={styles.sectionHeader}>{`${user.firstName}'s Timezone`}</Text>
        <View
          style={{
            width: "100%",
            paddingLeft: 20,
            paddingRight: 20,
            display: "flex",
            flexDirection: "row",
          }}
        >
          <View style={styles.section}>
            <Text style={styles.bio}>{user.timezone}</Text>
          </View>
        </View>
        <Text style={styles.sectionHeader}>{`${user.firstName}'s Region`}</Text>
        <View
          style={{
            width: "100%",
            paddingLeft: 20,
            paddingRight: 20,
            display: "flex",
            flexDirection: "row",
          }}
        >
          <View style={styles.section}>
            <Text style={styles.bio}>Maps API -> image</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  avatar: { borderRadius: 100, height: "100%", overflow: "visible", width: "100%" },
  avatarContainer: {
    backgroundColor: "white",
    borderColor: "white",
    borderRadius: 100,
    borderWidth: 4,
    height: 200,
    position: "absolute",
    top: "50%",
    width: 200,
  },
  bio: {
    color: "black",
  },
  cover: {
    alignItems: "center",
    backgroundColor: "grey",
    height: 150,
    overflow: "visible",
    width: "100%",
  },
  scroll: { backgroundColor: "white", height: "100%", width: "100%" },
  scrollContent: {
    alignItems: "center",
    flexDirection: "column",
    flexGrow: 1,
    paddingBottom: 50,
  },
  section: {
    backgroundColor: "white",
    borderColor: "rgba(100, 100, 100, 0.8)",
    borderRadius: 20,
    borderWidth: 2,
    marginBottom: 10,
    minHeight: 20,
    padding: 10,
    width: "100%",
  },
  sectionHeader: {
    alignSelf: "flex-start",
    color: "#373737",
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    marginBottom: 5,
    marginLeft: 30,
    marginTop: 0,
  },
  tag: {
    margin: 2,
  },
  tagsContainer: {
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
    marginBottom: 20,
    width: "70%",
  },
  tagsHeader: {
    color: "#373737",
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    marginBottom: 10,
    marginTop: "35%",
  },
})
