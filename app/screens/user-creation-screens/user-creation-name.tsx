import React, { useState } from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Button,
  KeyboardAvoidingView,
  Platform,
} from "react-native"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import { useNavigation } from "@react-navigation/native"
import { Text, GoalPicker, GoalTag } from "../../components"
import { color, spacing, typography } from "../../theme"
import { CognitoUserPool, CognitoUserAttribute } from "amazon-cognito-identity-js"
import { COGNITO_USER_POOL_ID, COGNITO_USER_POOL_WEB_CLIENT_ID } from "@env"
import { Api } from "../../services/api"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../models/store"
import { PrimaryParamList } from "../../navigation"
import { StackNavigationProp } from "@react-navigation/stack"
import { setUserData } from "../../models/reducers/user"
import AsyncStorage from "@react-native-async-storage/async-storage"

export const UserCreationName = () => {
  const navigation = useNavigation<StackNavigationProp<PrimaryParamList>>()
  const nextScreen = () => navigation.navigate("userCreationDate")
  const user = useSelector((state: RootState) => state.user)
  const firstName = useSelector((state: RootState) => state.user.firstName)
  const lastName = useSelector((state: RootState) => state.user.lastName)
  const dispatch = useDispatch()

  AsyncStorage.getAllKeys((err, keys) => {
    AsyncStorage.multiGet(keys, (error, stores) => {
      stores.map((result, i, store) => {
        console.log({ [store[i][0]]: store[i][1] })
        return true
      })
    })
  })
  console.log("Name:" + user)

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <View style={styles.inner}>
          <Text style={styles.heading}>Let's get started...</Text>
          <Text style={styles.subHeading}>What's your name?</Text>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholder="First name"
              value={firstName}
              placeholderTextColor={"#383232"}
              onChangeText={(name) => dispatch(setUserData({ firstName: name }))}
            />
            <TextInput
              style={styles.input}
              placeholder="Last name"
              value={lastName}
              placeholderTextColor={"#383232"}
              onChangeText={(name) => dispatch(setUserData({ lastName: name }))}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.button} onPress={nextScreen}>
          <Text style={styles.buttonText}>Next</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    borderColor: "#FFE5DF",
    borderRadius: 100,
    borderWidth: 4,
    height: 40,
    justifyContent: "center",
    paddingBottom: 5,
    width: 120,
  },
  buttonText: {
    color: "#FFE5DF",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
    textAlign: "center",
  },
  container: {
    alignItems: "center",
    backgroundColor: "#383232",
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  footer: {
    alignItems: "center",
    padding: 40,
  },
  heading: {
    color: "#FFE5DF",
    fontFamily: "Montserrat-Bold",
    fontSize: 60,
    width: "80%",
  },
  inner: {
    alignItems: "center",
    flex: 1,
    justifyContent: "space-evenly",
    width: "100%",
  },
  input: {
    backgroundColor: "#FFE5DF",
    borderRadius: 10,
    color: "#383232",
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    margin: spacing[2],
    width: "70%",
  },
  inputContainer: {
    alignItems: "center",
    width: "100%",
  },
  subHeading: {
    color: "#FFE5DF",
    fontFamily: "Montserrat-Bold",
    fontSize: 40,
    textAlign: "center",
    width: "80%",
  },
})
