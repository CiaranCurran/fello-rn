import React, { useEffect, useRef, useState } from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Button,
  KeyboardAvoidingView,
  Platform,
} from "react-native"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Text, GoalPicker, GoalTag } from "../../components"
import { color, spacing, typography } from "../../theme"
import { CognitoUserPool, CognitoUserAttribute } from "amazon-cognito-identity-js"
import { COGNITO_USER_POOL_ID, COGNITO_USER_POOL_WEB_CLIENT_ID } from "@env"
import api, { Api } from "../../services/api"
import { useDispatch } from "react-redux"
import { RootState } from "../../models/store"
import { PrimaryParamList } from "../../navigation"
import { StackNavigationProp } from "@react-navigation/stack"
import DateTimePicker from "@react-native-community/datetimepicker"
import { setUserData } from "../../models/reducers/user"

export const UserCreationGoals = () => {
  const navigation = useNavigation<StackNavigationProp<PrimaryParamList>>()
  const nextScreen = () => navigation.navigate("userCreationAvatar")
  const previousScreen = () => navigation.navigate("userCreationDate")
  const [goals, setGoals] = useState([])
  const [allGoals, setAllGoals] = useState([])
  const goalList = useRef<ScrollView>()
  const [disabled, setDisabled] = useState(true)
  const dispatch = useDispatch()

  const removeGoal = (goal) => {
    setGoals(goals.filter((item) => item.name !== goal.name))
  }

  useEffect(() => {
    // If there is at least one goal enable to the next button
    if (goals.length >= 1) {
      setDisabled(false)
    } else {
      setDisabled(true)
    }
  }, [goals])

  useEffect(() => {
    const fetchData = async () => {
      const response = await api.getGoals()
      if (response.kind === "ok") {
        setAllGoals(response.goals)
      }
    }
    fetchData()
  }, [])

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <View style={styles.inner}>
          <Text style={styles.subHeading}>What are your goals?</Text>
          <View style={styles.goalListContainer}>
            <ScrollView
              ref={goalList}
              horizontal={true}
              style={styles.goalList}
              onContentSizeChange={() => goalList.current?.scrollToEnd({ animated: true })}
              contentContainerStyle={styles.goalListContent}
            >
              {goals.map((goal, i) => {
                return (
                  <View
                    style={{
                      marginRight: 5,
                      height: "100%",
                      justifyContent: "center",
                    }}
                    key={i + "-goalList"}
                  >
                    <GoalTag name={goal.name} color={goal.color} onPress={removeGoal} />
                  </View>
                )
              })}
            </ScrollView>
          </View>
          <GoalPicker
            style={{ width: "80%", height: 220 }}
            textInputStyle={styles.goalPickerInput}
            listContainerStyle={styles.goalPickerList}
            onSelectSuggestion={(goal) => {
              dispatch(setUserData({ goals: goals.concat(goal) }))
              setGoals(goals.concat(goal))
            }}
            placeholderTextColor="#24322F"
            allGoals={allGoals.filter(
              (goal) => !goals.map((goal) => goal.name).includes(goal.name),
            )}
            onSubmitTextEntry={(goal) => {
              if (goal && !goals.map((goal) => goal.name).includes(goal)) {
                setGoals(goals.concat({ name: goal, color: color.randomColor() }))
              }
            }}
          />
        </View>
      </KeyboardAvoidingView>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.button} onPress={previousScreen}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={disabled ? { ...styles.button, ...styles.buttonDisabled } : styles.button}
          onPress={nextScreen}
          disabled={disabled}
        >
          <Text
            style={
              disabled ? { ...styles.buttonText, ...styles.buttonTextDisabled } : styles.buttonText
            }
          >
            Next
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    borderColor: "#F0FFC6",
    borderRadius: 100,
    borderWidth: 4,
    height: 40,
    justifyContent: "center",
    paddingBottom: 5,
    width: 120,
  },
  buttonDisabled: {
    borderColor: "#696e5b",
  },
  buttonFilled: {
    alignItems: "center",
    backgroundColor: "#F0FFC6",
    borderRadius: 10,
    height: 50,
    justifyContent: "center",
    marginTop: "20%",
    width: "80%",
  },
  buttonText: {
    color: "#F0FFC6",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
    textAlign: "center",
  },
  buttonTextDisabled: {
    color: "#696e5b",
  },
  buttonTextFilled: {
    color: "#24322F",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
  },
  container: {
    alignItems: "center",
    backgroundColor: "#24322F",
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  footer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingBottom: "10%",
    width: "100%",
  },
  goalList: {
    height: "100%",
    width: "100%",
  },
  goalListContainer: {
    height: 50,
    width: "80%",
  },
  goalListContent: {
    height: "100%",
  },
  goalPickerInput: {
    backgroundColor: "#F0FFC6",
    borderRadius: 10,
    color: "#24322F",
    fontFamily: "Montserrat-Bold",
    fontSize: 25,
    paddingLeft: spacing[3],
    padding: 0,
    paddingBottom: spacing[1],
    paddingTop: spacing[1],
  },
  goalPickerList: {
    backgroundColor: "#F0FFC6",
  },
  inner: {
    alignItems: "center",
    flex: 1,
    paddingTop: "20%",
    width: "100%",
  },
  subHeading: {
    color: "#F0FFC6",
    fontFamily: "Montserrat-Bold",
    fontSize: 40,
    width: "80%",
  },
})
