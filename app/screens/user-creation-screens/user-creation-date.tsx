import React, { useState } from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Button,
} from "react-native"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Text, GoalPicker, GoalTag } from "../../components"
import { color, spacing, typography } from "../../theme"
import { CognitoUserPool, CognitoUserAttribute } from "amazon-cognito-identity-js"
import { COGNITO_USER_POOL_ID, COGNITO_USER_POOL_WEB_CLIENT_ID } from "@env"
import { Api } from "../../services/api"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../models/store"
import { PrimaryParamList } from "../../navigation"
import { StackNavigationProp } from "@react-navigation/stack"
import DateTimePicker from "@react-native-community/datetimepicker"
import { setUserData } from "../../models/reducers/user"

export const UserCreationDate = () => {
  const navigation = useNavigation<StackNavigationProp<PrimaryParamList>>()
  const nextScreen = () => navigation.navigate("userCreationGoals")
  const previousScreen = () => navigation.navigate("userCreationName")

  const dispatch = useDispatch()
  const date = useSelector((state: RootState) => state.user.dateOfBirth)
  const [show, setShow] = useState(false)

  const showPicker = () => {
    setShow(true)
  }

  const changeDate = (event, selectedDate: Date) => {
    setShow(false)
    dispatch(setUserData({ dateOfBirth: selectedDate.toDateString() }))
  }

  return (
    <View style={styles.container}>
      <Text style={styles.subHeading}>When were you born?</Text>
      {show && (
        <DateTimePicker
          value={date ? new Date(date) : new Date()}
          testID={"dateTimePicker"}
          mode={"date"}
          maximumDate={new Date()}
          display={"calendar"}
          onChange={changeDate}
        />
      )}
      <TouchableOpacity style={styles.buttonFilled} onPress={showPicker}>
        <Text style={styles.buttonTextFilled}>{date || new Date().toDateString()}</Text>
      </TouchableOpacity>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.button} onPress={previousScreen}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={nextScreen}>
          <Text style={styles.buttonText}>Next</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    borderColor: "#C2BDE2",
    borderRadius: 100,
    borderWidth: 4,
    height: 40,
    justifyContent: "center",
    paddingBottom: 5,
    width: 120,
  },
  buttonFilled: {
    alignItems: "center",
    backgroundColor: "#C2BDE2",
    borderRadius: 10,
    height: 50,
    justifyContent: "center",
    marginTop: "20%",
    width: "80%",
  },
  buttonText: {
    color: "#C2BDE2",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
    textAlign: "center",
  },
  buttonTextFilled: {
    color: "#45243E",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
  },
  container: {
    alignItems: "center",
    backgroundColor: "#45243E",
    flex: 1,
    flexDirection: "column",
  },
  footer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginBottom: "10%",
    marginTop: "auto",
    width: "100%",
  },
  input: {
    backgroundColor: "#C2BDE2",
    borderRadius: 10,
    color: "#383232",
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    margin: spacing[2],
    width: "70%",
  },
  subHeading: {
    color: "#C2BDE2",
    fontFamily: "Montserrat-Bold",
    fontSize: 40,
    marginBottom: "5%",
    marginTop: "20%",
    width: "80%",
  },
})
