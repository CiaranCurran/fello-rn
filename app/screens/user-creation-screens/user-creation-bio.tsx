import React, { useState } from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Button,
  KeyboardAvoidingView,
  Platform,
} from "react-native"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Text, GoalPicker, GoalTag } from "../../components"
import { color, spacing, typography } from "../../theme"
import { CognitoUserPool, CognitoUserAttribute } from "amazon-cognito-identity-js"
import { COGNITO_USER_POOL_ID, COGNITO_USER_POOL_WEB_CLIENT_ID } from "@env"
import { Api } from "../../services/api"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../models/store"
import { PrimaryParamList } from "../../navigation"
import { StackNavigationProp } from "@react-navigation/stack"
import { setUserData } from "../../models/reducers/user"

export const UserCreationBio = () => {
  const navigation = useNavigation<StackNavigationProp<PrimaryParamList>>()
  const nextScreen = () => navigation.navigate("userCreationSubmit")
  const previousScreen = () => navigation.navigate("userCreationGoals")
  const firstName = useSelector((state: RootState) => state.user.firstName)
  const goals = useSelector((state: RootState) => state.user.goals)
  const dispatch = useDispatch()

  console.log(goals)
  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <View style={styles.inner}>
          <Text style={styles.heading}>...and finally a bio!</Text>
          <View style={styles.subSection}>
            <Text style={styles.subHeading}>Tell others something about yourself: </Text>
            <TextInput
              style={styles.input}
              placeholder={`Hey! I'm ${firstName} and I'm looking for an accountability partner to help with my goal of ${goals[0].name}...`}
              placeholderTextColor={"#849998"}
              multiline={true}
              numberOfLines={8}
              textAlignVertical="top"
              onChangeText={(text) => dispatch(setUserData({ bio: text }))}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.button} onPress={previousScreen}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableOpacity>
        <View style={styles.rightButtons}>
          <TouchableOpacity style={styles.button} onPress={nextScreen}>
            <Text style={styles.buttonText}>Next</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    borderColor: "#DFFFFD",
    borderRadius: 100,
    borderWidth: 4,
    height: 40,
    justifyContent: "center",
    paddingBottom: 5,
    width: 120,
  },
  buttonText: {
    color: "#DFFFFD",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
    textAlign: "center",
  },
  container: {
    alignItems: "center",
    backgroundColor: "#323838",
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  footer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingBottom: "10%",
    width: "100%",
  },
  heading: {
    color: "#DFFFFD",
    fontFamily: "Montserrat-Bold",
    fontSize: 40,
    marginTop: "10%",
    width: "90%",
  },
  inner: {
    alignItems: "center",
    flex: 1,
    justifyContent: "space-evenly",
    padding: 20,
    width: "100%",
  },
  input: {
    backgroundColor: "#DFFFFD",
    borderRadius: 10,
    color: "#323838",
    elevation: 5,
    fontFamily: "Montserrat-Bold",
    fontSize: 18,
    justifyContent: "flex-start",
    width: "90%",
  },
  rightButtons: {
    flexDirection: "column",
  },
  subHeading: {
    color: "#DFFFFD",
    fontFamily: "Montserrat-Bold",
    fontSize: 25,
    marginBottom: "5%",
    marginTop: "10%",
    width: "90%",
  },
  subSection: {
    alignItems: "center",
    marginBottom: "20%",
    width: "100%",
  },
})
function dispatch(arg0: any): void {
  throw new Error("Function not implemented.")
}
