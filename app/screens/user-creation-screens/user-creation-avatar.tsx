import React, { useEffect, useRef, useState } from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Button,
  KeyboardAvoidingView,
  Platform,
} from "react-native"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Text, GoalPicker, GoalTag } from "../../components"
import { color, spacing, typography } from "../../theme"
import { CognitoUserPool, CognitoUserAttribute } from "amazon-cognito-identity-js"
import { COGNITO_USER_POOL_ID, COGNITO_USER_POOL_WEB_CLIENT_ID } from "@env"
import api, { Api } from "../../services/api"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../models/store"
import { PrimaryParamList } from "../../navigation"
import { StackNavigationProp } from "@react-navigation/stack"
import DateTimePicker from "@react-native-community/datetimepicker"
import { setUserData } from "../../models/reducers/user"
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import ImagePicker, { ImageOrVideo } from "react-native-image-crop-picker"

export const UserCreationAvatar = () => {
  const navigation = useNavigation<StackNavigationProp<PrimaryParamList>>()
  const nextScreen = () => navigation.navigate("userCreationBio")
  const previousScreen = () => navigation.navigate("userCreationGoals")
  const [image, setImage] = useState<ImageOrVideo>()
  const [disabled, setDisabled] = useState(true)
  const avatarLocal = useSelector((state: RootState) => state.user.avatarLocal)
  const dispatch = useDispatch()

  const openPicker = () => {
    ImagePicker.openPicker({
      width: 400,
      height: 400,
      cropping: true,
      cropperCircleOverlay: true,
    }).then((image) => {
      console.log(JSON.stringify(image))
      dispatch(setUserData({ avatarLocal: image }))
      setImage(image)
    })
  }

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        style={styles.container}
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <View style={styles.inner}>
          <Text style={styles.subHeading}>Upload a profile picture</Text>
          <TouchableOpacity style={styles.circle} onPress={openPicker}>
            {image ? (
              <Image
                style={{ height: 200, width: 200 }}
                source={{ uri: "file:/" + image.path.split("file:///").join("") }}
              />
            ) : (
              <Icon name="camera-plus" size={100} color="#8E1515" />
            )}
          </TouchableOpacity>
          {image && <Text style={styles.greatMessage}>Looking great! 🎉</Text>}
        </View>
      </KeyboardAvoidingView>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.button} onPress={previousScreen}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={!image ? { ...styles.button, ...styles.buttonDisabled } : styles.button}
          onPress={nextScreen}
          disabled={!image}
        >
          <Text
            style={
              !image ? { ...styles.buttonText, ...styles.buttonTextDisabled } : styles.buttonText
            }
          >
            Next
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    borderColor: "#FF9E75",
    borderRadius: 100,
    borderWidth: 4,
    height: 40,
    justifyContent: "center",
    paddingBottom: 5,
    width: 120,
  },
  buttonDisabled: {
    borderColor: "#FF9E75",
  },
  buttonFilled: {
    alignItems: "center",
    backgroundColor: "#FF9E75",
    borderRadius: 10,
    height: 50,
    justifyContent: "center",
    marginTop: "20%",
    width: "80%",
  },
  buttonText: {
    color: "#FF9E75",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
    textAlign: "center",
  },
  buttonTextDisabled: {
    color: "#FF9E75",
  },
  buttonTextFilled: {
    color: "#24322F",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
  },
  circle: {
    alignItems: "center",
    backgroundColor: "#FF9E75",
    borderRadius: 200,
    elevation: 5,
    height: 200,
    justifyContent: "center",
    marginBottom: "20%",
    overflow: "hidden",
    width: 200,
  },
  container: {
    alignItems: "center",
    backgroundColor: "#8E1515",
    flex: 1,
    flexDirection: "column",
    width: "100%",
  },
  footer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-evenly",
    paddingBottom: "10%",
    width: "100%",
  },
  greatMessage: {
    color: "#FF9E75",
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    textAlign: "center",
    width: "80%",
  },
  inner: {
    alignItems: "center",
    flex: 1,
    justifyContent: "space-evenly",

    width: "100%",
  },
  subHeading: {
    color: "#FF9E75",
    fontFamily: "Montserrat-Bold",
    fontSize: 40,
    width: "80%",
  },
})
