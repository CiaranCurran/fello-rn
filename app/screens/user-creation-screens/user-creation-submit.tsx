import React, { useState } from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Button,
  KeyboardAvoidingView,
  Platform,
} from "react-native"
import { ScrollView, TextInput } from "react-native-gesture-handler"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Text, GoalPicker, GoalTag } from "../../components"
import { color, spacing, typography } from "../../theme"
import { CognitoUserPool, CognitoUserAttribute } from "amazon-cognito-identity-js"
import { COGNITO_USER_POOL_ID, COGNITO_USER_POOL_WEB_CLIENT_ID } from "@env"
import api from "../../services/api"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "../../models/store"
import { PrimaryParamList } from "../../navigation"
import { StackNavigationProp } from "@react-navigation/stack"
import { setUserData } from "../../models/reducers/user"
import { setCognitoUserData } from "../../models/reducers/auth"

export const UserCreationSubmit = () => {
  const navigation = useNavigation<StackNavigationProp<PrimaryParamList>>()
  const dispatch = useDispatch()
  const user = useSelector((state: RootState) => state.user)
  const email = useSelector((state: RootState) => state.auth.cognitoUsername)

  const nextScreen = async () => {
    dispatch(setUserData({ email: email }))
    const response = await api.createUser(user)
    console.log(response)
    if (response.kind === "ok") {
      console.log("appa" + JSON.stringify(response.user))
      dispatch(setUserData({ avatar: response.user.avatar, _id: response.user._id }))
      dispatch(setCognitoUserData({ userCreationComplete: true }))
      navigation.navigate("home")
    } else {
      // TODO: PUT SOME WARNING HERE
      console.log("Failed to create user")
    }
  }
  const previousScreen = () => navigation.navigate("userCreationName")
  const firstName = useSelector((state: RootState) => state.user.firstName)

  console.log("first " + firstName)
  return (
    <KeyboardAvoidingView
      style={styles.container}
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      enabled={Platform.OS === "ios"}
    >
      <View style={styles.inner}>
        <Text style={styles.heading}>You're all done!</Text>
        <Text style={styles.subHeading}>
          You can edit your profile at any time and even add more information later if you'd like.
          {"\n\n"}For now...
        </Text>
        <Text style={styles.welcome}>Welcome to the fello community!</Text>
      </View>
      <View style={styles.footer}>
        <TouchableOpacity style={styles.button} onPress={previousScreen}>
          <Text style={styles.buttonText}>Back</Text>
        </TouchableOpacity>
        <View>
          <TouchableOpacity style={{ ...styles.button, ...styles.submit }} onPress={nextScreen}>
            <Text style={{ ...styles.buttonText, color: "#01B3FF" }}>Submit</Text>
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAvoidingView>
  )
}

const styles = StyleSheet.create({
  button: {
    alignItems: "center",
    borderColor: "#FFE5DF",
    borderRadius: 100,
    borderWidth: 4,
    height: 40,
    justifyContent: "center",
    paddingBottom: 5,
    width: 120,
  },
  buttonText: {
    color: "#FFE5DF",
    fontFamily: "Montserrat-Bold",
    fontSize: 30,
    textAlign: "center",
  },
  container: {
    alignItems: "center",
    backgroundColor: "#383232",
    flex: 1,
    flexDirection: "column",
  },
  footer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginBottom: "10%",
    width: "100%",
  },
  heading: {
    color: "#ffe5df",
    fontFamily: "Montserrat-Bold",
    fontSize: 60,
    marginTop: "10%",
    width: "80%",
  },
  inner: { alignItems: "center", flex: 1, width: "100%" },
  input: {
    backgroundColor: "#FFE5DF",
    borderRadius: 10,
    color: "#383232",
    fontFamily: "Montserrat-Bold",
    fontSize: 20,
    margin: spacing[2],
    width: "70%",
  },
  subHeading: {
    color: "#FFE5DF",
    fontFamily: "Montserrat-Bold",
    fontSize: 25,
    marginTop: "10%",
    width: "80%",
  },
  submit: {
    borderColor: "#01B3FF",
    width: 200,
  },
  welcome: {
    color: "#01B3FF",
    fontFamily: "Montserrat-Bold",
    fontSize: 35,
    marginTop: "5%",
    width: "80%",
  },
})
