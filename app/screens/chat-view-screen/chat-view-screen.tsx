import { observer } from "mobx-react-lite"
import React, { createRef, useEffect, useLayoutEffect, useRef, useState } from "react"
import { StyleSheet, View } from "react-native"
import { AvatarButton, Header } from "../../components"
import { GiftedChat } from "react-native-gifted-chat"
import Icon from "react-native-vector-icons/Ionicons"
import { useNavigation } from "@react-navigation/native"
import { io } from "socket.io-client"

export const ChatViewScreen = observer(function ChatsScreen() {
  useEffect(() => {
    const socket = io("http://192.168.1.96:5000")
    socket.on("connect", () => {
      socket.emit("message", { chatID: "abc", message: "Hello World", sender: "id123" })
    })
  }, [])
  const navigation = useNavigation()

  const backButtonPress = () => {
    navigation.goBack()
  }

  return (
    <View style={{ height: "100%", marginBottom: 0 }}>
      <Header
        title="Name Here"
        leftComponent={
          <Icon
            style={{ marginLeft: 20 }}
            name={"arrow-back"}
            size={30}
            color={"white"}
            onPress={backButtonPress}
          />
        }
      />
      <GiftedChat />
    </View>
  )
})
