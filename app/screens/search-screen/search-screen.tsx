import React from "react"
import {
  View,
  Image,
  ViewStyle,
  TextStyle,
  ImageStyle,
  SafeAreaView,
  TextInput,
  StyleSheet,
} from "react-native"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import {
  AvatarButton,
  Button,
  Header,
  Screen,
  Text,
  Wallpaper,
  SearchBar,
  SearchList,
} from "../../components"
import { color, spacing, typography } from "../../theme"
import useToggle from "../../utils/toggle"
import Icon from "react-native-vector-icons/Ionicons"
import { types } from "mobx-state-tree"
import profileImg from "../../../assets/images/avatar2.jpeg"

interface SearchHeaderButtonsProps {
  onPress: () => void
}

const SearchHeaderButtons = observer(function SearchHeaderButtons(props: SearchHeaderButtonsProps) {
  const { onPress } = props
  const onClick = () => {
    onPress()
  }

  return (
    <View style={styles.buttonsView}>
      <Icon style={styles.button} name="md-search" size={30} color="white" onPress={onClick} />
      <Icon style={styles.button} name="md-filter" size={30} color="white" />
    </View>
  )
})

interface SearchBarProps {}

export const SearchScreen = observer(function SearchScreen() {
  const navigation = useNavigation()
  const nextScreen = () => navigation.navigate("demo")
  const [searchIsOn, toggleSearch] = useToggle(true)

  return (
    <View style={{ height: "100%" }}>
      {searchIsOn ? (
        <Header
          title="Search"
          leftComponent={<AvatarButton style={styles.avatarButton} />}
          rightComponent={<SearchHeaderButtons onPress={toggleSearch} />}
        />
      ) : (
        <Header leftComponent={<SearchBar onClose={toggleSearch} />} />
      )}
      {/* SEARCH LIST */}
      <View style={{ flex: 1 }}>
        <SearchList></SearchList>
      </View>
    </View>
  )
})

const styles = StyleSheet.create({
  avatarButton: {},
  button: {
    padding: spacing[1],
  },
  buttonsView: {
    display: "flex",
    flexDirection: "row",
    marginLeft: "auto",
  },
})
