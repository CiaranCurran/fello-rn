import React from "react"
import { View, Image, ViewStyle, TextStyle, ImageStyle, SafeAreaView } from "react-native"
import { useNavigation } from "@react-navigation/native"
import { observer } from "mobx-react-lite"
import { Button, Header, Screen, Text, Wallpaper } from "../../components"
import { color, spacing, typography } from "../../theme"
import { CognitoUserPool, CognitoUserAttribute } from "amazon-cognito-identity-js"
import { COGNITO_USER_POOL_ID, COGNITO_USER_POOL_WEB_CLIENT_ID } from "@env"

export const LoginScreen = observer(function LoginScreen() {
  const navigation = useNavigation()
  const nextScreen = () => navigation.navigate("demo")

  return (
    <View>
      <Text>"This is the login screen"</Text>
    </View>
  )
})
