import AWS from "aws-sdk"
import { S3_ACCESS_KEY, S3_SECRET_KEY, S3_BUCKET_REGION } from "@env"

const credentials = new AWS.Credentials({
  accessKeyId: String(S3_ACCESS_KEY),
  secretAccessKey: String(S3_SECRET_KEY),
})
const awsConfig = new AWS.Config({
  credentials: credentials,
  region: String(S3_BUCKET_REGION),
})
const s3 = new AWS.S3(awsConfig)

export default s3
