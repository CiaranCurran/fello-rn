/**
 * Welcome to the main entry point of the app. In this file, we'll
 * be kicking off our app or storybook.
 *
 * Most of this file is boilerplate and you shouldn't need to modify
 * it very often. But take some time to look through and understand
 * what is going on here.
 *
 * The app navigation resides in ./app/navigation, so head over there
 * if you're interested in adding screens and navigators.
 */
import "./i18n"
import "./utils/ignore-warnings"
import React, { useEffect, useRef } from "react"
import { NavigationContainerRef } from "@react-navigation/native"
import { SafeAreaProvider, initialWindowSafeAreaInsets } from "react-native-safe-area-context"
import * as storage from "./utils/storage"
import {
  useBackButtonHandler,
  RootNavigator,
  canExit,
  setRootNavigation,
  useNavigationPersistence,
} from "./navigation"
import { Provider, useSelector } from "react-redux"
import { persistStore } from "redux-persist"
import { PersistGate } from "redux-persist/integration/react"
import { store } from "./models/store"
import RNBootSplash from "react-native-bootsplash"
import { color } from "./theme"

// This puts screens in a native ViewController or Activity. If you want fully native
// stack navigation, use `createNativeStackNavigator` in place of `createStackNavigator`:
// https://github.com/kmagiera/react-native-screens#using-native-stack-navigator
import { enableScreens } from "react-native-screens"
import { StatusBar } from "react-native"
import socket from "./services/sockets"

// setup api
import Api from "./services/api"
import SocketManager from "./services/sockets/socket-manager"

enableScreens()
Api.setup()

export const NAVIGATION_PERSISTENCE_KEY = "NAVIGATION_STATE"

// create persisted store
const persistor = persistStore(store)

/**
 * This is the root component of our app.
 */
function App() {
  const navigationRef = useRef<NavigationContainerRef>()

  setRootNavigation(navigationRef)
  useBackButtonHandler(navigationRef, canExit)
  const { initialNavigationState, onNavigationStateChange } = useNavigationPersistence(
    storage,
    NAVIGATION_PERSISTENCE_KEY,
  )

  // Kick off initial async loading actions, like loading fonts and RootStore
  useEffect(() => {
    const init = async () => {}

    const hideSplash = async () => {
      await RNBootSplash.hide({ fade: true })
      RNBootSplash.getVisibilityStatus().then((status) => console.log(status))
      StatusBar.setBackgroundColor(color.statusBar, true)
    }

    init().finally(hideSplash)
  }, [])

  // Before we show the app, we have to wait for our state to be ready.
  // In the meantime, don't render anything. This will be the background
  // color set in native by rootView's background color. You can replace
  // with your own loading component if you wish.
  if (!store) return null

  // otherwise, we're ready to render the app
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <SocketManager>
          <SafeAreaProvider initialSafeAreaInsets={initialWindowSafeAreaInsets}>
            <RootNavigator
              ref={navigationRef}
              initialState={initialNavigationState}
              onStateChange={onNavigationStateChange}
            />
          </SafeAreaProvider>
        </SocketManager>
      </PersistGate>
    </Provider>
  )
}

export default App
