import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import AsyncStorage from "@react-native-async-storage/async-storage"
import RNLocalize from "react-native-localize"
import { persistReducer } from "redux-persist"

export enum UserType {
  Free = "free",
  Premium = "premium",
  Admin = "admin",
  SuperAdmin = "super-admin",
}

export type Goal = {
  name: string
  color: string
  _id: string
  count: number
}

export type Connection = {
  _id: string
  createdAt: number
}

/* eslint-disable camelcase */
export interface User {
  _id?: string
  firstName?: string
  lastName?: string
  dateOfBirth?: string
  goals?: [Goal]
  bio?: string
  email?: string
  timezone?: string
  avatar?: string
  avatarLocal?: any
  userType?: UserType
  rooms?: string[]
  connections?: {
    friends: [Connection]
    sent: [Connection]
    received: [Connection]
  }
}

// Get user preferenced timezone *not based on location*
const timezone = RNLocalize.getTimeZone()

const initialState: User = {
  _id: undefined,
  firstName: "",
  lastName: "",
  dateOfBirth: undefined,
  goals: undefined,
  bio: undefined,
  email: undefined,
  timezone: timezone,
  avatar: undefined,
  avatarLocal: undefined,
  userType: UserType.Free,
  rooms: [],
}

const userConfig = {
  key: "user",
  version: 1,
  storage: AsyncStorage,
}

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserData: (state, action: PayloadAction<User>) => {
      state = { ...state, ...action.payload }
      return state
    },
  },
})

// Action creators are generated for each case reducer function
export const { setUserData } = userSlice.actions

export default persistReducer(userConfig, userSlice.reducer)
