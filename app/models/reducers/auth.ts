import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import AsyncStorage from "@react-native-async-storage/async-storage"
import { persistReducer } from "redux-persist"

/* eslint-disable camelcase */
export interface Auth {
  cognitoUsername?: string
  cognitoAuthSession?: object
  cognitoUserIsVerified?: boolean
  userCreationComplete?: boolean
}

const initialState: Auth = {
  cognitoUsername: "",
  cognitoAuthSession: null,
  cognitoUserIsVerified: false,
  userCreationComplete: false,
}

const authConfig = {
  key: "auth",
  version: 1,
  storage: AsyncStorage,
}

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setCognitoUserData: (state, action: PayloadAction<Auth>): Auth => {
      state = { ...state, ...action.payload }
      return state
    },
  },
})

// Action creators are generated for each case reducer function
export const { setCognitoUserData } = authSlice.actions

export default persistReducer(authConfig, authSlice.reducer)
