#!/bin/bash
#To use run `chmod u+r+x toggle_storybook.sh`
if [[ $1 == "on" ]];
then
    term="SHOW_STORYBOOK = false"
    replace="SHOW_STORYBOOK = true"
    sed -i "s/$term/$replace/" index.js
fi
if [[ $1 == "off" ]];
then
    term="SHOW_STORYBOOK = true"
    replace="SHOW_STORYBOOK = false"
    sed -i "s/$term/$replace/" index.js
fi